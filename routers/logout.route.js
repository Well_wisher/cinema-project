const router = require("express").Router();

//Controllers
const { logout } = require("../controllers/logout.controller");

router.get("/", logout);

module.exports = router;

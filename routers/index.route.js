const router = require('express').Router()

//Controllers
const {home} = require('../controllers/index.controller')

router.get('/*', home);

module.exports = router;
const router = require('express').Router();

// Controllers
const {user} = require('../controllers/user.controller')
router.get('/', user)

module.exports = router
const router = require("express").Router();

//Controllers
const { login } = require("../controllers/login.controller");

router.post("/", login);

module.exports = router;

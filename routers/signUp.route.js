const router = require("express").Router();

//Controllers
const { signUp } = require("../controllers/signUp.controller");

router.post("/", signUp);

module.exports = router;

const bcrypt = require("bcrypt");
const User = require("../models/user.model");

exports.signUp = async function(req, res) {
  const { name, email, password } = req.body;
  const candidate = await User.findOne({ email });

  if (!candidate) {
    const user = new User({ name, email, password });
    user.password = await bcrypt.hash(password, 10);
    user.save((err, user) => {
      if (err) res.json({ error: true });
      else {
        req.session.user = name;
        res.json({ save: true });
      }
    });
  } else {
    res.json({ save: false });
  }
};

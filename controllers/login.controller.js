const User = require("../models/user.model");
// const passport = require("passport");
const bcrypt = require("bcrypt");

exports.login = async function(req, res) {
  const user = await User.findOne({ email: req.body.email });

  if (user) {
    const pass = await bcrypt.compare(req.body.password, user.password);
    if (pass) {
      req.session.user = user.name;
      req.user = user.name
      res.json({ error: false, user: user.name });
    } else {
      res.json({ error: true, mess: "Вы ввели не верный пароль" });
    }
  } else {
    res.json({ error: true, mess: "Такой пользователь не зарегистрирован" });
  }
};


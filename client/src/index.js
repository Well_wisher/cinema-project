import React from "react";
import ReactDOM from "react-dom";

import "./style/style.scss";
import App from "./components/App";
import(/* webpackChunkName: 'serviceWorker' */ '../sw')

ReactDOM.render(<App />, document.getElementById("wrapper"));
// if ("serviceWorker" in navigator) {
//   navigator.serviceWorker
//     .register("../sw.js")
//     .then(function(reg) {
//       console.log("Successfully registered service worker", reg);
//     })
//     .catch(function(err) {
//       console.warn("Error whilst registering service worker", err);
//     });
// }

// KEY:  6071f0a5
//  http://www.omdbapi.com/?apikey=6071f0a5&I=tt0111161

// =========== Search 250 top https://www.imdb.com/chart/top?ref_=nv_mv_250 ===========
// const a = document.querySelectorAll(".titleColumn a")
// const idAll = []
// a.forEach(a => {
//   let id = a.href.match(/tt(\d)+/g)
//   if(id) idAll.push(id.join(''))
// });
// console.log(idAll);

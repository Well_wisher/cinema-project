import React, { createContext, Component } from "react";
import { top250, popular, soon } from "./store";
import { getFavotite } from "../helpers/favoriteStore";

export const MoviesContext = createContext();

export default class MoviesProvider extends Component {
  ON_PAGE = 10;
  state = {
    db: "top250",
    query: "i", //i-ID;s-search name
    search: "",
    movies: [],
    temp: [],
    isLoaded: false,
    step: 2,
    page: 0,
    totalResults: 0,
    isMore: false,
    isResponse: false,
    isError: ""
  };
  componentDidMount() {
    this.setState({ movies: [], totalResults: 250, page: 0 });
    this.fetchMovies(top250);
  }

  fetchMovies = async dbName => {
    let db = [];
    switch (dbName) {
      case "top250":
        db = top250;
        break;
      case "popular":
        db = popular;
        break;
      case "soon":
        db = soon;
        break;
      case "favorite":
        db = getFavotite();
        break;
      default:
        db = top250;
        break;
    }
    const { page, step } = this.state;
    const section = db.slice(
      page * this.ON_PAGE,
      db.length >= page * this.ON_PAGE
        ? page === 0
          ? step * this.ON_PAGE
          : page * this.ON_PAGE + step * this.ON_PAGE
        : db.length
    );

    this.setState({ isLoaded: true });
    if (section.length) {
      section.forEach(async (id, i) => {
        await this.getMovies(id);

        if (i === section.length - 1) {
          setTimeout(() => {
            this.setState(state => ({
              movies: state.temp,
              page: state.page + state.step,
              isLoaded: false
            }));
          }, 100);
        }
      });
    } else {
      if (dbName === "favorite") {
        this.setState({ isError: "Favorites empty" });
      }
    }
  };

  getMovies = async data => {
    this.setState({ isError: false });
    const { query, step, page, totalResults } = this.state;
    try {
      const respons = await fetch(
        `http://www.omdbapi.com/?apikey=6071f0a5&${query}=${data}&page=${page}`
      );
      const movie = await respons.json();

      if (movie.Response === "True") {
        if (movie.Search) {
          this.setState(state => ({
            isResponse: true,
            isLoaded: false,
            movies: state.movies.concat(movie.Search),
            totalResults: movie.totalResults
          }));

          if (this.state.totalResults > page * 10 + 1) {
            if (this.state.movies.length % (step * 10) != 0) {
              this.setState(state => ({ page: state.page + 1 }));
              this.getMovies(data);
            }
          }
        } else {
          this.setState(state => ({
            isError: "",
            isResponse: true,
            temp: [...state.temp, movie]
          }));
        }

        if (
          totalResults > page * this.ON_PAGE + 1 &&
          totalResults > step * this.ON_PAGE
        ) {
          this.setState({ isMore: true });
        } else {
          this.setState({ isMore: false });
        }
      }
      if (movie.Response === "False") {
        this.setState({
          isError: movie.Error,
          isResponse: false
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  onSearch = async title => {
    await this.setState({
      query: "s",
      movies: [],
      temp: [],
      search: title,
      page: 1
    });
    this.getMovies(title);
  };

  onMore = () => {
    if (this.state.query === "i") {
      this.fetchMovies(this.state.db);
    } else {
      Promise.resolve(
        this.setState({ page: (this.state.page += 1), isLoaded: true })
      ).then(() => this.getMovies(this.state.search));
    }
  };

  changeListMovies = async list => {
    await this.setState({
      query: "i",
      temp: [],
      movies: [],
      search: "",
      page: 0
    });
    switch (list) {
      case "top":
        this.setState({ totalResults: 250, db: "top250" });
        this.fetchMovies("top250");
        break;
      case "popular":
        this.setState({ totalResults: 100, db: "popular" });
        this.fetchMovies("popular");
        break;
      case "soon":
        this.setState({ totalResults: 30, db: "soon" });
        this.fetchMovies("soon");
        break;
      case "favorite":
        this.setState({ totalResults: getFavotite().length, db: "favorite" });
        this.fetchMovies("favorite");
        break;
      default:
        this.setState({ totalResults: 250, db: "top250" });
        this.fetchMovies("top250");
        break;
    }
  };

  render() {
    const { movies, isLoaded, step, isMore, isError, db } = this.state;
    return (
      <MoviesContext.Provider
        value={{
          db,
          movies,
          isLoaded,
          step,
          isMore,
          isError,
          onMore: this.onMore,
          changeListMovies: this.changeListMovies,
          search: this.onSearch
        }}
      >
        {this.props.children}
      </MoviesContext.Provider>
    );
  }
}

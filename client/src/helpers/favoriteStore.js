export const getFavotite = () => {
  if(localStorage.getItem('_pf-f')){
    const db = localStorage.getItem('_pf-f')
    return JSON.parse(db)
  }
  return []
}
export const setFavotire = id => {
  if(localStorage.getItem('_pf-f')) {
    localStorage.setItem('_pf-f', JSON.stringify([...getFavotite(), id]))
  }else{
    localStorage.setItem('_pf-f', JSON.stringify([id]))
  }
}
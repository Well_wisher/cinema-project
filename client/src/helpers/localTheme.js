import LocalStore from "./localStore";

export default function () {
  const theme = LocalStore.getStore()
  if(theme === 'dark'){
    document.querySelector("html").setAttribute("data-theme", "dark");
  }else{
    document.querySelector("html").setAttribute("data-theme", "light");
  }
}
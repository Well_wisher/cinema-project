import axios from "axios";
import React from "react";

// export const getUser = () => {
//   const [user, setUser] = React.useState("");
//   React.useEffect(() => {
//     const fetchUser = async () => {
//       try {
//         const res = await axios.get("/api/user");
//         setUser(res.data.user);
//       } catch (error) {
//         console.error(error);
//       }
//     };
//     fetchUser();
//   }, []);
//   return user;
// };

export const fetchUser = async () => {
  try {
    const res = await axios.get("/api/user");
    return res.data.user;
  } catch (error) {
    console.error(error);
  }
};

export const login = data => {
  try {
    return axios
      .post("/login", { data })
      .then(user => {
        if (user) return true;
        else return false;
      })
      .catch(() => false);
  } catch {
    return "Error from the server, during user login";
  }
};

export const signUp = data => {
  try {
    return axios
      .post("/sign-up", { data })
      .then(user => {
        if (user) return "";
        else return "";
      })
      .catch(() => false);
  } catch {
    return "Error from the server, during user sign up";
  }
};


import { useEffect, useState } from "react";
import Placeholder from '../images/poster-background.jpg'

const useLoaderImage = poster => {
  const [imageIsReady, setImageIsReady] = useState(false);
  const [imagePoster, setImagePoster] = useState("");

  useEffect(() => {
    const img = new Image();
    if (poster === "N/A") {
      img.src = Placeholder;
    } else {
      img.src = poster;
    }
    img.onload = () => {
      setImageIsReady(true);
      setImagePoster(img.src);
    };
    img.onerror = () => {
      img.src = Placeholder;
      setImagePoster(Placeholder);
    };
  }, []);
  return { imageIsReady, imagePoster };
};
export default useLoaderImage;

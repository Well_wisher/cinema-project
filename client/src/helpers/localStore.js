class LocalStore {
  static setStore(newTheme) {
    localStorage.setItem('cf_theme', newTheme)
  }
  static getStore() {
    const theme = localStorage.getItem('cf_theme');
    if(theme) {
      if(theme === 'light' || 'dark') return theme
      return 'light'
    }else{
      localStorage.setItem('cf_theme', 'light')
      return 'light'
    }
  }
}

export default LocalStore


import React, {useContext} from "react";
import { MoviesContext } from '../../Store/moviesProvider';

import ViewLineIcon from "../../images/icon-lines.svg";
import ViewCardIcon from "../../images/icon-card2.svg";

const Breadcrumbs = ({ setViewCard }) => {
  const {changeListMovies} = useContext(MoviesContext);

  const list = e => {
    e.preventDefault();
    changeListMovies(e.target.dataset.list)
  }
  return (
    <div className="container">
      <div className="cf-breadcrumbs">
        <nav>
          <ol>
            <li>
              <a href="/" data-list="popular" onClick={list}>Most Popular</a>
            </li>
            <li>
              <a href="/" data-list="top" onClick={list}>Top Rated</a>
            </li>
            <li>
              <a href="/" data-list="soon" onClick={list}>Coming Soon</a>
            </li>
          </ol>
        </nav>
        <div className="cf-breadcrumbs-views">
          <button id="cf-breadcrumbs-card" onClick={() => setViewCard(true)}>
            <ViewCardIcon/>
          </button>
          <button id="cf-breadcrumbs-line" onClick={() => setViewCard(false)}>
            <ViewLineIcon />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Breadcrumbs;

import React from "react";

import Logo from "../images/icon-logo.svg";

const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div>
          <div className="cf-logo">
            <span>
              <Logo />
            </span>
            <h1>Poster Films</h1>
          </div>
          <p>© Copyright 2019</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;

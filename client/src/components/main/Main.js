import React, { useContext } from "react";

import Card from "./Card";
import PageError from "../Error/Error-info";
import LoaderIcon from "../../images/loader.svg";
import Spinner from "../../images/icon-spinner.svg";

import { MoviesContext } from "../../Store/moviesProvider";

const Main = ({ viewCard }) => {
  const { movies, isMore, isLoaded, onMore, isError } = useContext(
    MoviesContext
  );
  const onCard = e => {
    const checkContains = target => {
      if(e.currentTarget != target){
        if(target.classList.contains("more-icon") || target.classList.contains('cf-card_reveal')) {
          e.preventDefault()
        }else{
          checkContains(target.parentElement)
        }
      }
    }
    checkContains(e.target)
  };
  return !isError ? (
    <main>
      {movies.length ? (
        <div className="container">
          <div id={viewCard ? "cards" : "cards-list"}>
            {movies.map(movie => (
              <a
                href={`https://www.imdb.com/title/${movie.imdbID}`}
                key={`${movie.Year}${movie.imdbID}`}
                className="cf-card"
                title={movie.Title}
                onClick={onCard}
              >
                <Card movie={movie} />
              </a>
            ))}
          </div>
        </div>
      ) : (
        <div className="loading">
          <LoaderIcon />
        </div>
      )}
      {isMore ? (
        <div className="pagination">
          <button type="button" disabled={isLoaded} onClick={() => onMore()}>
            Загрузить еще {isLoaded && <Spinner />}
          </button>
        </div>
      ) : null}
    </main>
  ) : (
    <PageError />
  );
};

export default Main;

import React, { useState, useRef, useContext } from "react";

import MoreIcon from "../../images/icon-more.svg";
import BookmarkIcon from "../../images/bookmark.svg";
import LoaderIcon from "../../images/loader.svg";
import { getFavotite, setFavotire } from "../../helpers/favoriteStore";
import useLoaderImage from "../../helpers/useLoaderImage";
import { MoviesContext } from "../../Store/moviesProvider";

const Card = ({ movie }) => {
  const { changeListMovies, db: favorite } = useContext(MoviesContext);
  const { imageIsReady, imagePoster } = useLoaderImage(movie.Poster);
  const cardReveal = useRef(null);
  const [btn, setBtn] = useState(true);

  const moreDescription = () => {
    if (cardReveal.current.classList.contains("reveal-hover")) {
      cardReveal.current.classList.remove("reveal-hover");
    } else {
      cardReveal.current.classList.add("reveal-hover");
    }
  };

  return (
    <>
      <div className="cf-card_image">
        <div className="poster">
          {imageIsReady ? (
            <img src={imagePoster} alt={movie.Title} />
          ) : (
            <div className="loading">
              <LoaderIcon />
            </div>
          )}
        </div>
        <span>
          <button type="button" className="more-icon" onClick={moreDescription}>
            <MoreIcon fill="#000000" />
          </button>
        </span>
      </div>
      <div className="cf-card_content">
        <div>
          <span className="movie-title">{movie.Title}</span>
          {movie.imdbRating && movie.imdbRating !== "N/A" && (
            <small className="movie-rating">{movie.imdbRating}</small>
          )}
        </div>
        <p className="movie-genre">{movie.Genre}</p>
      </div>
      <div className="cf-card_reveal" ref={cardReveal}>
        {favorite !== "favorite" && (
          <div className="btn-favorite" data-favorite={btn}>
            {getFavotite().some(favoriteId => favoriteId === movie.imdbID) ? (
              <button
                type="button"
                onClick={() => changeListMovies("favorite")}
              >

                <BookmarkIcon />
                <span>Favorite</span>
              </button>
            ) : (
              <button
                type="button"
                className="add-favorite"
                onClick={() => {
                  setFavotire(movie.imdbID);
                  setBtn(false);
                }}
              >
                + Favorite
              </button>
            )}
          </div>
        )}

        <p>{movie.Plot}</p>
      </div>
    </>
  );
};

export default Card;

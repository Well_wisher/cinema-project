import React, { useState, useEffect, Suspense, lazy } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Header from "./header/Header";
import Breadcrumbs from "./breadcrumbs/Breadcrumbs";
import Main from "./main/Main";
import Footer from "./Footer";
import localTheme from "../helpers/localTheme";
import LoaderIcon from "../images/loader.svg";
import ErrorBoundary from "./Error/ErrorBoundary";

import MoviesProvider from "../Store/moviesProvider";

const SignUp = lazy(() =>
  import(/* webpackChunkName: 'signUp' */ "./sign-in-up/SignUp")
);
const Login = lazy(() =>
  import(/* webpackChunkName: 'login' */ "./sign-in-up/login")
);
const Page404 = lazy(() =>
  import(/* webpackChunkName: 'page-404' */ "./Error/Page404")
);

const App = () => {
  const [viewCard, setViewCard] = useState(true);
  const [user, setUser] = useState(true);
  const [isBreadcrumbs, setIsBreadcrumbs] = useState(true);
  useEffect(() => {
    // Get theme from localStore Api
    localTheme();
    // Get User from session
    // const fetchUser = async () => {
    //   try {
    //     const res = await axios.get("/api/user");
    //     setUser(res.data.user);
    //   } catch (error) {
    //     console.error(error);
    //   }
    // };
    // fetchUser();
  }, []);

  return (
    <Router>
      <MoviesProvider>
          <Header user={user} setUser={setUser} />
          {isBreadcrumbs && <Breadcrumbs setViewCard={setViewCard} />}
          <ErrorBoundary>
            <Suspense
              fallback={
                <div className="loading">
                  <LoaderIcon />
                </div>
              }
            >
              <Switch>
                <Route
                  exact
                  path="/"
                  render={props => {
                    setIsBreadcrumbs(true);
                    return <Main {...props} viewCard={viewCard} />
                }}
                />
                <Route
                  path="/sign-up"
                  render={props => {
                    setIsBreadcrumbs(false);
                    return <SignUp {...props} setUser={setUser} />;
                  }}
                />
                <Route
                  exact
                  path="/login"
                  render={props => {
                    setIsBreadcrumbs(false)
                    return <Login {...props} setUser={setUser} />
                }}
                />
                <Route component={Page404} />
              </Switch>
            </Suspense>
          </ErrorBoundary>
          <Footer />
      </MoviesProvider>
    </Router>
  );
};

export default App;

import React from "react";
import { Link } from "react-router-dom";

const Page404 = () => (
  <div className="container" id="notfound">
    <div className="notfound">
      <div className="notfound-wrap">
        <h2>
          4<span>0</span>4
        </h2>
      </div>
      <p>
        The page you are looking for might have been removed had its name
        changed or is temporarily unavailable.
      </p>
      <Link to="/">home page</Link>
    </div>
  </div>
);

export default Page404;

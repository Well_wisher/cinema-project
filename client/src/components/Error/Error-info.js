import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { MoviesContext } from "../../Store/moviesProvider";

const PageError = () => {
  const { isError, changeListMovies } = useContext(MoviesContext);
  return (
    <div className="container" id="notfound">
      <div className="notfound">
        {/* <div className="notfound-wrap">
          <h2>
            5<span>0</span>0
          </h2>
        </div> */}
        <p style={{ fontSize: "2rem" }}>{isError}</p>
        <button type="button" onClick={()=>changeListMovies('top')}>home page</button>
      </div>
    </div>
  );
};

export default PageError;

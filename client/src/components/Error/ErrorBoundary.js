import React, { Component } from 'react'

export default class ErrorBoundary extends Component {
  state = {
    hasError: false
  }
  static getDerivedStateFromError(error){
    this.setState({hasError: true})
  }
  componentDidCatch() {
    // Reporting error
  }
  render() {
    if(this.state.hasError) {
      return (
        <h2>Произошла ошибка</h2>
      )
    }
    return this.props.children;
  }
}

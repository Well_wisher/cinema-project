const lightTheme = {
  bg: "#ffffff",
  colorMain: "#d8232a",
  colorTextDark: "#3f444e",
  colorTextGray: "#989ca4",
  colorTextWhite: "#ffffff"
};

const darkTheme = {
  bg: "#2c1320",
  colorMain: "#d8232a",
  colorTextDark: "#3f444e",
  colorTextGray: "#989ca4",
  colorTextWhite: "#ffffff"
};

export { lightTheme, darkTheme };

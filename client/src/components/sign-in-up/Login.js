import React from "react";
import PropsTypes from "prop-types";
import { withRouter } from "react-router";
import axios from "axios";

import TextInput from "./TextInput";
import validate from "../../helpers/validate";

class Login extends React.Component {
  state = {
    flashMessage: "",
    formControls: {
      email: {
        value: "",
        valid: false,
        touched: false,
        validationRules: {
          isRequired: true,
          isEmail: false
        }
      },
      password: {
        value: "",
        valid: false,
        touched: false,
        validationRules: {
          minLength: 6,
          isRequired: true
        }
      }
    }
  };

  changeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;

    const updatedControls = {
      ...this.state.formControls
    };
    const updatedFormElement = {
      ...updatedControls[name]
    };
    updatedFormElement.value = value;
    updatedFormElement.touched = true;
    updatedFormElement.valid = validate(
      value,
      updatedFormElement.validationRules
    );

    updatedControls[name] = updatedFormElement;

    this.setState({
      formControls: updatedControls
    });
  };

  formSubmitHandler = async e => {
    e.preventDefault();
    const { email, password } = this.state.formControls;

    if (email.valid === true && password.valid === true) {
      try {
        const user = await axios.post("/login", {  
          email: email.value,
          password: password.value
        });
        if(user.data.error){
          this.setState({flashMessage: user.data.mess});
        }
        if (!user.data.error && user.data.user) {
          this.props.setUser(user.data.user);
          this.props.history.push("/");
        }
      } catch (error) {
        this.setState({flashMessage: "Произошла ошибка при отправке данных"});
        console.error("Login error: ", error);
      }
    }
  };

  render() {
    const { email, password } = this.state.formControls;
    return (
      <div className="container forms">
        <div className="wrap">
          <form className="validate-form" method="POST" action="/login">
            <span className="forms-title">Авторизоваться</span>
            {!!this.state.flashMessage && (
              <p className="flash-massage">{this.state.flashMessage}</p>
            )}

            <TextInput
              type="email"
              name="email"
              value={email.value}
              onChange={this.changeHandler}
              placeholder="EMAIL"
              isValid={email.valid}
              errorText="Email не валидный"
            />
            <TextInput
              type="password"
              name="password"
              value={password.value}
              onChange={this.changeHandler}
              placeholder="PASSWORD"
              isValid={password.valid}
              errorText="Пароль меньше 6 символов"
              minLength="6"
              maxLength="50"
            />

            <div className="forms-btn">
              <button type="submit" onClick={this.formSubmitHandler}>
                Авторизоваться
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
Login.propTypes = {
  setUser: PropsTypes.func
};

export default withRouter(Login);

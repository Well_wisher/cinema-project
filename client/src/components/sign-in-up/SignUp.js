import React from "react";
import PropsTypes from "prop-types";
import { withRouter } from 'react-router'

import TextInput from "./TextInput";
import validate from "../../helpers/validate";

class SignUp extends React.Component {
  state = {
    formControls: {
      name: {
        value: "",
        valid: false,
        touched: false,
        validationRules: {
          minLength: 6,
          isRequired: true
        }
      },
      email: {
        value: "",
        valid: false,
        touched: false,
        validationRules: {
          isRequired: true,
          isEmail: false
        }
      },
      password: {
        value: "",
        valid: false,
        touched: false,
        validationRules: {
          minLength: 6,
          isRequired: true
        }
      }
    },
    flashMessage: ""
  };
  changeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;

    const updatedControls = {
      ...this.state.formControls
    };
    const updatedFormElement = {
      ...updatedControls[name]
    };
    updatedFormElement.value = value;
    updatedFormElement.touched = true;
    updatedFormElement.valid = validate(
      value,
      updatedFormElement.validationRules
    );

    updatedControls[name] = updatedFormElement;

    this.setState({
      formControls: updatedControls
    });
  };

  formSubmitHandler = async e => {
    e.preventDefault();
    const { name, email, password } = this.state.formControls;
    if (
      name.valid === true &&
      email.valid === true &&
      password.valid === true
    ) {
      try {
        const res = await fetch("/sign-up", {
          method: "POST",
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          body: JSON.stringify({
            name: name.value,
            email: email.value,
            password: password.value
          })
        });
        const data = await res.json();

        if (!data.error) {
          if(data.save){
            
            this.props.setUser(name.value);
            this.props.history.push('/')
          }else{
            this.setState({ flashMessage: "Такой пользователь уже есть" });
          }
        } else {
          this.setState({ flashMessage: "Error" });
        }
      } catch (err) {
        console.error(err);
      }
    }
  };

  render() {
    const { name, email, password } = this.state.formControls;
    return (
      <div className="container forms">
        <div className="wrap">
          <form className="validate-form" method="POST" action="/sign-up">
            <span className="forms-title">Регистрация</span>
            {!!this.state.flashMessage && (
              <p className="flash-massage">{this.state.flashMessage}</p>
            )}

            <TextInput
              type="text"
              name="name"
              value={name.value}
              onChange={this.changeHandler}
              placeholder="NAME"
              isValid={name.valid}
              errorText="Ваше имя меньше 6 символов"
              minLength="6"
              maxLength="50"
            />
            <TextInput
              type="email"
              name="email"
              value={email.value}
              onChange={this.changeHandler}
              placeholder="EMAIL"
              isValid={email.valid}
              errorText="Email не валидный"
            />
            <TextInput
              type="password"
              name="password"
              value={password.value}
              onChange={this.changeHandler}
              placeholder="PASSWORD"
              isValid={password.valid}
              errorText="Пароль меньше 6 символов"
              minLength="6"
              maxLength="50"
            />

            <div className="forms-btn">
              <button type="submit" onClick={this.formSubmitHandler}>
                Регистрация
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
SignUp.propTypes = {
  setUser: PropsTypes.func
};

export default withRouter(SignUp);

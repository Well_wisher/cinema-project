import React, { useState } from "react";
import PropTypes from "prop-types";

const TextInput = ({
  name,
  type,
  value,
  placeholder,
  onChange,
  isValid,
  errorText,
  ...rest
}) => {
  const [isError, setisError] = useState(false);
  const inputRef = React.useRef();

  const onBlurInp = e => {
    if (e.target.value.trim() !== "") {
      inputRef.current.classList.add("has-val");
    } else {
      inputRef.current.classList.remove("has-val");
    }
    if (!isValid) setisError(true);
  };

  return (
    <div className="wrap-input validate-input">
      <input
        className="input"
        type={type}
        name={name}
        value={value}
        onChange={onChange}
        onBlur={onBlurInp}
        onFocus={() => setisError(false)}
        ref={inputRef}
        {...rest}
        required
      />
      <span className="focus-input" data-placeholder={placeholder} />
      {isError && <span className="isErrorInput">{errorText}</span>}
    </div>
  );
};

TextInput.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  isValid: PropTypes.bool.isRequired,
  errorText: PropTypes.string.isRequired,
  rest: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool
  ])
};

export default TextInput;

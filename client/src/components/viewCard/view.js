import React from 'react'

const view = ({ match }) => {
  return (
    <div className="container">
      <h3>ID: {match.params.id}</h3>
    </div>
  )
}

export default view

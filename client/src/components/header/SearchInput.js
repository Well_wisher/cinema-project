import React, { useState, useContext } from "react";
import IconSearch from "../../images/icon-search.svg";
import { MoviesContext } from "../../Store/moviesProvider";

const SearchInput = () => {
  const { search } = useContext(MoviesContext);
  const [value, setValue] = useState("");

  const handleChange = e => {
    setValue(e.target.value);
  };
  const handleSubmit = e => {
    e.preventDefault();
    setValue("")
    search(value.trim());
  };
  return (
    <form onSubmit={handleSubmit} className="cf-search-input">
      <input
        type="search"
        name="search"
        value={value}
        onChange={handleChange}
        onBlur={() => setValue("")}
        placeholder="Movie Title"
      />
      <button type="submit">
        <IconSearch />
      </button>
    </form>
  );
};

export default SearchInput;

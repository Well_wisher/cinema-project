import React from "react";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import { Animated } from "react-animated-css";

import MobileSearch from "./MobileSearch";
import Office from "./Office";
import HamburgerIcon from "./HamburgerIcon";
import MobileMenu from "./MobileMenu";

import Logo from "../../images/icon-logo.svg";

const Header = ({user, setUser}) => {
  const [toggleMenu, setToggleMenu] = React.useState(false);
  const [toggleSearch, setToggleSearch] = React.useState(false);

  return (
    <header className="cf-header">
      <div className="container">
        <HamburgerIcon
          toggleMenu={toggleMenu}
          ontoggleMenu={() => setToggleMenu(!toggleMenu)}
        />
        <div className="cf-logo">
          <span>
            <Logo />
          </span>
          <h1><Link to="/">Poster Films</Link></h1>
        </div>
        <Office toggleSearch={toggleSearch} setToggleSearch={setToggleSearch} user={user.toString()} setUser={setUser} />
        {toggleSearch && (
          <Animated
            animationIn="fadeInDown"
            animationOut="fadeOutUp"
            animationInDuration={300}
            animationOutDuration={300}
            isVisible={toggleSearch}
            className="cf-search"
          >
            <MobileSearch toggleSearch={setToggleSearch}/>
          </Animated>
        )}
      </div>
      {toggleMenu && <MobileMenu isMenu={toggleMenu} toggleMenu={setToggleMenu} />}
    </header>
  );
};

Header.propTypes = {
  user: PropTypes.string
}

export default Header;

import React, { useState, useEffect, useContext, useRef } from "react";
import { MoviesContext } from "../../Store/moviesProvider";
import IconSearch from "../../images/icon-search.svg";

const MobileSearch = ({toggleSearch}) => {
  const { search } = useContext(MoviesContext);
  const [value, setValue] = useState("");
  const input = useRef();
  useEffect(() => {
    input.current.focus();
  }, []);
  const handleChange = e => {
    setValue(e.target.value);
  };
  const handleSubmit = () => {
    setValue("");
    search(value.trim());
    toggleSearch(false)
  };
  const handleEnter = e => {
    if(e.key === 'Enter') handleSubmit()
  }
  return (
    <>
      <input
        type="search"
        name="search"
        id="mobile-search"
        ref={input}
        onChange={handleChange}
        onBlur={() => setValue("")}
        onKeyPress={handleEnter}
      />
      <button type="button" onClick={handleSubmit}>
        <IconSearch />
      </button>
    </>
  );
};

export default MobileSearch;

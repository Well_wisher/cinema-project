import React, {useContext} from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import axios from 'axios'

import SearchInput from "./SearchInput";
import LocalStore from "../../helpers/localStore";
import { MoviesContext } from '../../Store/moviesProvider'

import SearchIcon from "../../images/icon-search.svg";
import AvatarIcon from "../../images/avatar.svg";
import DarkIcon from "../../images/dark-theme.svg";
import LoginIcon from "../../images/icon-login.svg";
import LogoutIcon from "../../images/icon-logout.svg";
import SignUpIcon from "../../images/icon-sign-up.svg";

const Office = ({ toggleSearch, setToggleSearch, user, setUser }) => {
  const {changeListMovies} = useContext(MoviesContext);
  
  const ontoggleTheme = () => {
    const html = document.querySelector("html");
    const theme = html.getAttribute("data-theme");
    if (theme === "dark") {
      html.setAttribute("data-theme", "light");
      LocalStore.setStore("light");
    } else {
      html.setAttribute("data-theme", "dark");
      LocalStore.setStore("dark");
    }
  };
  const logout = async e =>{
    e.preventDefault();
    try {
      const res = await axios.get('/logout')
      if(res.data.logout) setUser('')
    } catch (error) {
      console.error('Logout: ', error)
    }
  }
  return (
    <div className="cf-office">
      <SearchInput />
      <button
        type="button"
        className="btn-mob-search"
        onClick={() => setToggleSearch(!toggleSearch)}
      >
        <SearchIcon />
      </button>
      <button onClick={ontoggleTheme}>
        <DarkIcon />
      </button>
      {Boolean(user) && (
        <div className="account" onClick={()=>changeListMovies('favorite')} title={user.toString()}>
          <AvatarIcon />
          <span id="user" className="office-text">
            {user}
          </span>
        </div>
      )}

      {Boolean(!user) && (
        <>
          <Link to="/login" id="login" title="Login">
            <LoginIcon />
            <span className="office-text">Login</span>
          </Link>
          <Link to="/sign-up" id="sign-up" title="Sign Up">
            <SignUpIcon />
            <span className="office-text">Sign up</span>
          </Link>
        </>
      )}

      {Boolean(user) && (
        <Link to="/logout" id="logout" title="LogOut" onClick={logout}>
          <LogoutIcon />
          <span className="office-text">LogOut</span>
        </Link>
      )}
    </div>
  );
};
Office.propTypes = {
  toggleSearch: PropTypes.bool,
  setToggleSearch: PropTypes.func,
  user: PropTypes.string,
  setUser: PropTypes.func
};

export default Office;

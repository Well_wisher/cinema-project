import React from "react";

const HamburgerIcon = ({ toggleMenu, ontoggleMenu }) => (
  <div className="hamburger-wrap">
    <div
      className={`hamburger ${toggleMenu ? "is-active" : ""}`}
      id="hamburger"
      onClick={ontoggleMenu}
    >
      <span className="line" />
      <span className="line" />
      <span className="line" />
    </div>
  </div>
);

export default HamburgerIcon;

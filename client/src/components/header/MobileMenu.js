import React, { useContext } from "react";
import useLockBodyScroll from "../../helpers/useLockBodyScroll";
import { MoviesContext } from "../../Store/moviesProvider";

const MobileMenu = ({ isMenu, toggleMenu }) => {
  const { changeListMovies } = useContext(MoviesContext);
  useLockBodyScroll();
  const list = e => {
    e.preventDefault();
    toggleMenu(false);
    changeListMovies(e.target.dataset.list);
  };
  return (
    <nav className={`menu-mobile ${isMenu ? "menu-mobile_active" : ""}`}>
      <ol>
        <li>
          <a href="/" data-list="popular" onClick={list}>
            Most Popular
          </a>
        </li>
        <li>
          <a href="/" data-list="top" onClick={list}>
            Top Rated
          </a>
        </li>
        <li>
          <a href="/" data-list="soon" onClick={list}>
            Coming Soon
          </a>
        </li>
      </ol>
    </nav>
  );
};

export default MobileMenu;

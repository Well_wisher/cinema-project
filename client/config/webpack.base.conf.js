const path = require("path");
const fs = require("fs");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const MediaQueryPlugin = require("media-query-plugin");
const SpriteLoaderPlugin = require("svg-sprite-loader/plugin");

const PATHS = {
  src: path.join(__dirname, "../src"),
  dist: path.join(__dirname, "../dist")
  // static: "static/"
};
console.log(path.resolve(__dirname, '..', 'manifest.json'));

const HTML_PAGES = fs
  .readdirSync(path.join(PATHS.src, "/pages"))
  .filter(page => page);

exports.baseWebpack = {
  externals: {
    paths: PATHS
  },
  entry: {
    app: PATHS.src
  },
  output: {
    filename: `js/[name].[hash:6].js`,
    path: PATHS.dist,
    chunkFilename: "js/[name].[chunkhash].js",
    publicPath: "/"
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        react: {
          test: /[\\/]node_modules[\\/](react)[\\/]/,
          name: "react",
          chunks: "all",
          minSize: 0
        },
        reactDom: {
          test: /[\\/]node_modules[\\/](react-dom)[\\/]/,
          name: "react-dom",
          chunks: "all",
          minSize: 0
        },
        vendor: {
          test: /[\\/]node_modules[\\/](!react)(!rect-dom)[\\/]/,
          name: "vendor",
          chunks: "all",
          minSize: 0
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      // {
      //   test: /^icon-.*\.svg$/,
      //   use: [
      //     {
      //       loader: "svg-sprite-loader",
      //       options: {
      //         // extract: true,
      //         spriteFilename: 'sprites'
      //       }
      //     },
      //     {
      //       loader: "svgo-loader",
      //       options: {
      //         plugins: [
      //           { removeTitle: true },
      //           { convertColors: { shorthex: false } },
      //           { convertPathData: false }
      //         ]
      //       }
      //     }
      //   ]
      // },
      // {
      //   test: /\.svg$/,
      //   exclude: /^icon-.*\.svg$/,
      //   loader: "file-loader",
      //   options: {
      //     name: `images/[name].[ext]`
      //   }
      // },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              // Inline files smaller than 10 kB
              limit: 10 * 1024,
            },
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                // enabled: false,
                // NOTE: mozjpeg is disabled as it causes errors in some Linux environments
                // Try enabling it in your environment by switching the config to:
                enabled: true,
                progressive: true,
              },
              gifsicle: {
                interlaced: false,
              },
              optipng: {
                optimizationLevel: 7,
              },
              pngquant: {
                quality: '65-90',
                speed: 4,
              },
            },
          },
          // {
          //   loader: "file-loader",
          //   options: {
          //     name: `images/[name].[ext]`
          //   }
          // }
        ]
      },
      // {
      //   test: /\.(woff|woff2|ttf|otf|eot)(\?v=\d+\.\d+\.\d+)?$/,
      //   loader: "file-loader",
      //   options: {
      //     name: `fonts/[name].[ext]`
      //   }
      // },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: /node_modules/,
        use: [
          { loader: "style-loader" },
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              // modules: true,
              // importLoaders: 1,
              // localIdentName: "[name]__[local]--[hash:base64:5]",
              sourceMap: true,
              // minimize: true
            }
          },
          MediaQueryPlugin.loader,
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true,
              config: { path: path.join(__dirname, "/config") }
            }
          },
          {
            loader: "sass-loader",
            options: { sourceMap: true }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      "~": "src"
    }
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: `css/[name].[hash:6].css`,
      chunkFilename: `css/[name].[hash:6].css`
    }),
    new CopyWebpackPlugin([
      { from: `${PATHS.src}/images`, to: `${PATHS.dist}/images` },
      { from: `${PATHS.src}/fonts`, to: `${PATHS.dist}/fonts` },
      { from: `${PATHS.src}/favicon`, to: `${PATHS.dist}/favicon` },
      { from: path.resolve(__dirname, '..', 'manifest.json'), to: `${PATHS.dist}/` }
    ]),
    new SpriteLoaderPlugin({ plainSprite: true }),
    ...HTML_PAGES.map(
      page =>
        new HtmlWebpackPlugin({
          template: `${PATHS.src}/pages/${page}`,
          filename: `./${page}`
        })
    )
  ]
};

exports.PATHS = PATHS;

const webpack = require('webpack')
const merge = require('webpack-merge')
const path = require('path')
const {baseWebpack} = require('./webpack.base.conf')

module.exports = merge(baseWebpack, {
  mode: "development",
  devtool: "cheap-module-eval-source-map",
  devServer: {
    contentBase: baseWebpack.externals.paths.dist,
    port: 4001,
    contentBase: path.join(__dirname, '../src'),
    open: true,
    hot: true,
    historyApiFallback: true,
    overlay: {
      warnings: false,
      errors: true
    }
  },
  plugins: [
    new webpack.SourceMapDevToolPlugin({
      filename: '[file].map'
    })
  ]
})
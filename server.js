require("dotenv").config();
const express = require("express");
const path = require("path");
const cors = require("cors");
const helmet = require("helmet");
const cookieParser = require("cookie-parser");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const passport = require("passport");

const mongodbURI = process.env.MONGODB_URI;
const app = express();
// Passport config
require("./config/passport")(passport);

mongoose
  .connect(mongodbURI, { useCreateIndex: true, useNewUrlParser: true })
  .then(() => console.log("Connected to MongoDB..."))
  .catch(err => console.error("Could not connect to MongoDB...", err));

app.use(cors());
app.use(helmet());
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(express.static(path.join(__dirname, "client/dist")));
app.use(
  session({
    secret: process.env.RANDOM_SECRET_WORD,
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({ url: mongodbURI })
  })
);
// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  console.log("user: ", req.sessionID);
  console.log("user2: ", req.session.user);
  console.log("user3: ", req.user);

  if (req.isAuthenticated()) {
    // res.locals.user = req.user;
    console.log("auth");
    next();
  } else {
    console.log("no auth");
    next();
    // res.locals.user = null;
  }
});

// Routers
app.use("/login", require("./routers/login.route"));
app.use("/logout", require("./routers/logout.route"));
app.use("/sign-up", require("./routers/signUp.route"));
app.use("/api/user", require("./routers/user.route"));
// app.use("/", require("./routers/index.route"));

const port = process.env.PORT || 4000;
app.listen(port);
